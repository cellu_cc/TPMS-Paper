import numpy as np
#import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
#from mpl_toolkits.mplot3d.art3d import Line3DCollection
#from scipy.spatial import cKDTree 
#import networkx as nx
#import itertools

def apply_symmetry(innodes,inbars,inusedbars,orders,mirrors,translations):
	outnodes = np.copy(innodes)
	for i in range(3):
		outnodes.T[i] = mirrors[i]*(innodes.T[orders[i]])+translations[i]
		for j in range(4):
			if outnodes[j][i] < 0.0:
				outnodes[j][i] = outnodes[j][i]+1.0
			if outnodes[j][i] >= 1.0:
				outnodes[j][i] = outnodes[j][i]-1.0
	unchanged_nodes = 0
	for i in range(4):
		if np.all(np.abs(outnodes[i]-innodes[i])<0.01):
			unchanged_nodes=unchanged_nodes+1

	outbars = np.copy(inbars)
	for i in range(3):
		outbars.T[i] = mirrors[i]*(inbars.T[orders[i]])+translations[i]
		for j in range(np.shape(inbars)[0]):
			if outbars[j][i] < 0.0:
				outbars[j][i] = outbars[j][i]+1.0
			if outbars[j][i] >= 1.0:
				outbars[j][i] = outbars[j][i]-1.0
	
	unusedbars = []
	for j in range(np.shape(inbars)[0]):
		if j not in inusedbars:
			unusedbars.append(j)
	
	unchanged_bars = 0
	for i in inusedbars:
		if np.all(np.abs(outbars[i]-inbars[i])<0.01):
			unchanged_bars=unchanged_bars+1
		for j in unusedbars:
			if np.all((outbars[i]-inbars[j]==0.0)):
				print("INVALID SYMMETRY OP: ", inbars[i], inbars[j])

	return unchanged_nodes, unchanged_bars

nodes = np.array([[0.00,0.00,0.00],
				  [0.00,0.50,0.50],
				  [0.50,0.00,0.50],
				  [0.50,0.50,0.00]])

bars  = np.array([[0.00,0.25,0.25],  #00
				  [0.00,0.25,0.75],  #01
				  [0.00,0.75,0.25],  #02
				  [0.00,0.75,0.75],  #03
				  [0.25,0.00,0.25],  #04
				  [0.25,0.00,0.75],  #05
				  [0.25,0.25,0.00],  #06
				  [0.25,0.25,0.50],  #07
				  [0.25,0.50,0.25],  #08
				  [0.25,0.50,0.75],  #09
				  [0.25,0.75,0.00],  #10
				  [0.25,0.75,0.50],  #11
				  [0.50,0.25,0.25],  #12
				  [0.50,0.25,0.75],  #13
				  [0.50,0.75,0.25],  #14
				  [0.50,0.75,0.75],  #15
				  [0.75,0.00,0.25],  #16
				  [0.75,0.00,0.75],  #17
				  [0.75,0.25,0.00],  #18
				  [0.75,0.25,0.50],  #19
				  [0.75,0.50,0.25],  #20
				  [0.75,0.50,0.75],  #21
				  [0.75,0.75,0.00],  #22
				  [0.75,0.75,0.50]]) #23

#unused_bars = [0,3,4,8,9,10,12,15,17,18,19,21]
used_bars = [1, 2, 5, 9, 10, 11, 13, 14, 16, 18, 19, 20]

print(' 1 ( E): +x+0.0, +y+0.0, +z+0.0 : ',apply_symmetry(nodes,bars,used_bars,[0,1,2],[ 1, 1, 1],[0.0,0.0,0.0]))
print(' 5 (C3): +z+0.0, +x+0.0, +y+0.0 : ',apply_symmetry(nodes,bars,used_bars,[2,0,1],[ 1, 1, 1],[0.0,0.0,0.0]))
print('18 (C2): -x+0.0, +z+0.5, +y+0.5 : ',apply_symmetry(nodes,bars,used_bars,[0,2,1],[-1, 1, 1],[0.0,0.5,0.5]))
print('15 (C4): +y+0.5, -x+0.0, +z+0.5 : ',apply_symmetry(nodes,bars,used_bars,[1,0,2],[ 1,-1, 1],[0.5,0.0,0.5]))
print(' 2 (C2): +x+0.0, -y+0.5, -z+0.5 : ',apply_symmetry(nodes,bars,used_bars,[0,1,2],[ 1,-1,-1],[0.0,0.5,0.5]))
print('25 ( I): -x+0.0, -y+0.0, -z+0.0 : ',apply_symmetry(nodes,bars,used_bars,[0,1,2],[-1,-1,-1],[0.0,0.0,0.0]))
print('39 (S4): -y+0.5, +x+0.0, -z+0.5 : ',apply_symmetry(nodes,bars,used_bars,[1,0,2],[-1, 1,-1],[0.5,0.0,0.5]))
print('29 (S3): -z+0.0, -x+0.0, -y+0.0 : ',apply_symmetry(nodes,bars,used_bars,[2,0,1],[-1,-1,-1],[0.0,0.0,0.0]))
print('26 (gh): +x+0.5, +y+0.5, -z+0.0 : ',apply_symmetry(nodes,bars,used_bars,[0,1,2],[ 1, 1,-1],[0.5,0.5,0.0]))
print('38 (sd): +y+0.0, +x+0.0, +z+0.0 : ',apply_symmetry(nodes,bars,used_bars,[1,0,2],[ 1, 1, 1],[0.0,0.0,0.0]))

#print('37 (sd): -y+0.5, -x+0.5, +z+0.0 : ',apply_symmetry(nodes,bars,used_bars,[1,0,2],[-1,-1, 1],[0.5,0.5,0.0]))

#print(' 3: ',apply_symmetry(nodes,[0,1,2],[-1, 1,-1],[0.5,0.0,0.5]))
#print(' 4: ',apply_symmetry(nodes,[0,1,2],[-1,-1, 1],[0.5,0.5,0.0]))
#print(' 6: ',apply_symmetry(nodes,[2,0,1],[-1,-1, 1],[0.5,0.5,0.0]))
#print(' 7: ',apply_symmetry(nodes,[2,0,1],[ 1,-1,-1],[0.0,0.5,0.5]))
#print(' 8: ',apply_symmetry(nodes,[2,0,1],[-1, 1,-1],[0.5,0.0,0.5]))
#print(' 9: +x+0.0, -y+0.5, -z+0.5 : ',apply_symmetry(nodes,[1,2,0],[ 1, 1, 1],[0.0,0.0,0.0]))
#print('13: +x+0.5, -z+0.0, +y+0.5 : ',apply_symmetry(nodes,[0,2,1],[-1,-1,-1],[0.0,0.0,0.0]))
#print('28:  x+0.5,  y+0.5, -z+0.0 : ',apply_symmetry(nodes,[0,1,2],[ 1, 1,-1],[0.5,0.5,0.0]))

#print('28 (sd):  +y+0.0, +x+0.0, +z+0.0 : ',apply_symmetry(nodes,[1,0,2],[ 1, 1, 1],[0.0,0.0,0.0]))

#print('11: ',apply_symmetry(nodes,[2,0,1],[-1, 1,-1],[0.5,0.0,0.5]))
#print('12: ',apply_symmetry(nodes,[2,0,1],[-1, 1,-1],[0.5,0.0,0.5]))




'''

bars = np.array([[ 0, 2],
				 [ 0, 5],
				 [ 2, 4],
				 [ 2, 6],
				 [ 2, 8],
				 [ 3, 6],
				 #[ 3, 8],
				 #[ 4, 7],
				 [ 5, 6],
				 [ 5,10],
				 [ 5,11],
				 [ 6, 9],
				 [ 7, 8],
				 #[ 7,10],
				 [ 7,11]#,
				 #[ 8,13],
				 #[ 9,11],
				 #[11,13]
				 ])

nodes.T[0] = -nodes.T[0]+1.0
nodes.T[1] = -nodes.T[1]+1.0
nodes.T[2] = nodes.T[2]


nodes = np.vstack((nodes,nodes+[1.0,0,0]))
bars = np.vstack((bars,bars+[14,14]))

nodes = np.vstack((nodes,nodes+[0,1.0,0]))
bars = np.vstack((bars,bars+[28,28]))

nodes = np.vstack((nodes,nodes+[0,0.0,1.0]))
bars = np.vstack((bars,bars+[56,56]))


fig = plt.figure(figsize=(12,12))

ax = fig.add_subplot(1, 1, 1, projection='3d')

line_segments = []
for bar in bars:
    line_segments.append([nodes[bar[0]],nodes[bar[1]]])
line_segments = np.array(line_segments)

line_segments = Line3DCollection(line_segments)
    
ax.scatter(nodes.T[0],nodes.T[1],nodes.T[2])
ax.add_collection(line_segments)

ax = fig.add_subplot(1, 2, 2, projection='3d')

nodes.T[0] = nodes.T[0]
nodes.T[1] = 0.5-nodes.T[1]
nodes.T[2] = 0.5-nodes.T[2]

line_segments = []
for bar in bars:
    line_segments.append([nodes[bar[0]],nodes[bar[1]]])
line_segments = np.array(line_segments)

line_segments = Line3DCollection(line_segments)
    
ax.scatter(nodes.T[0],nodes.T[1],nodes.T[2])
ax.add_collection(line_segments)

plt.show()
'''