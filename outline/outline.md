# Outline

## Introduction

## Background
  1. Architected materials are an important development in the field of mechanical metamaterials, producing high stiffness and strength structures at unprecedented density regimes.
  2. The typical approach of architected materials is to use the edges of a polyhedral foam to dictate the location of a three-dimensional periodic lattice of struts
    * this approach has its roots in the development of architected materials as the product of filling an uncured polymer with gas and then letting the resulting shape cure into a rigid structure. 
        + The bubbles would, in an ideal case, form a geometry that maximized volume while minimizing surface area.
    * A variety of geometries can be derived from the class of polyhedral tesselations of three-dimensional space
  3. The properties of a given geometry will relate the stiffness of the lattice relative to its constituent material to the density of the lattice relative to that same material. 
    * This relationship takes the form of $E^*/E = k(p^*/p)^a$, where $a$ is typically an integer constant, and $k$ is a variable that changes based on loading direction, material defects, and manufacturing process.
    * the highest-performing of the cellular solids is the octet truss, with an $a$ of 1
    * Deshpande et. al. were the first to derive the compliance matrix for this lattice, as well as the Von Mises stress surface
  4. Since its proposal, the octet truss has been constructed out of a variety of materials, including nickle-phosphorus (Ni-P), silica, titanium (Ti-6Al-4V), and carbon fiber reinforced polymer (CFRP).
  5. The conventional method for assessing a lattice has been to partition the space into bending- and stretching-dominated materials.
    * bending-dominated materials transfer loads through induced moments. 
    * stretching-dominated materials transfer loads through axial tension and compression of the beams.
    * The performance of the stretching-dominated materials is higher than those that exhibit bending-dominated behavior since the properties of the load transfer depend only on the cross sectional area of the beam in the former case, while in the latter they depend on both the cross-sectional area and the beam length. 
  6. Approaches for assessing the bending- and stretching- behavior of a lattice have progressed from Maxwell's proposal of the rigidity criterion in the mid-19th century.
    * node coordination, which is limited to structures that have translationally-invariant nodes
    * periodic counting rules, which calculate only the sum of the mechanism and self-stress states 
    * symmetry-extended rigidity equation, which provides the full decomposition of the mechanism and self-stress states as a linear combination of irreducible representations of the space group to which the structure belongs.
    * multiscale scheme, which relates the stiffness matrix of the unit cell of the lattice to the stiffness matrix of the metamaterial composed by the geometry. 
  7. With the exception of the multiscale scheme, all of the other methods assume that the structure is a pin-jointed truss. 
    * The rationale of this approach is that a mechanism state in a pin-jointed truss corresponds to bending-dominated behavior in a cellular solid
  8. We present evidence of a class of structures that challenge this connection between mechanism states and bending-dominated behavior


## Methodology

### Possible Theories
  1. The plate provides sufficient constraint that the lattice-plate structure is rigid
    * Do an FEA analysis, but load each edge node individually- don't simulate a plate
    * doesnt explain Vigliotti Pasini performance (no plate, infinite lattice)
  2. The lattice is composed of isotruss planes that transfer loads to one another
    * cuboct, for instance, is interlocking kagome lattices
  3. 
## Results

## Discussion

## Conclusion