# Triply Periodic Minimal Surfaces

This paper will describe the generation and analysis of Triply Periodic Minimal Surfaces. 

  * `overleaf` contains the git submodule corresponding to the Overleaf paper (in $\LaTeX$)
  * `media` contains raw media files associated with generating figures for the `overleaf` document
  * `outline` contains outlines and notes for the paper


